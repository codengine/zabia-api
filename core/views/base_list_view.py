from django.views.generic.list import ListView


class BaseListView(ListView):
    paginate_by = 5
    context_object_name = "object_list"