from engine.exceptions.ce_exception import CEException


class AjaxResponseFormatMixin(object):
    response = {
        "status": "FAILURE",
        "data": None,
        "message": None,
        "code": None
    }

    def ensure_status(self, data):
        if data and not data.get('status'):
            raise CEException("Ajax response does not contain status field")
        return data
