from enum import Enum


class ConversationType(Enum):
    ZABIA_INTRODUCTION = "ZABIA_INTRODUCTION"
    GEO_REF = "GEO_REF"
    GREETING = "GREETING"
    BASIC_INFO = "BASIC_INFO"
    SCHEDULER_Q = "SCHEDULER_Q"
    CHAT_Q = "CHAT_Q"
    MISCELLENEOUS = "MISCELLENEOUS"


class UserResponseType(Enum):
    CHOICE_RESPONSE = "CHOICE_RESPONSE"
    TEXT_RESPONSE = "TEXT_RESPONSE"
    GRAPH = "GRAPH"
    MAP = "MAP"


class ChatTriggerSource(Enum):
    ZABIA_USER = "ZABIA_USER"
    ZABIA_SCHEDULER = "ZABIA_SCHEDULER"


class Presentation(Enum):
    INFO = "info"
    CAROUSEL = "carousel"
    LINK = "link"
    LIST = "list"
    REPLY = "reply"
    TEXT = "text"
    ACTIONS = "actions"


class ConversationTag(Enum):
    NAME = "name"
    AGE = "age"
    GENDER = "gender"
    HEIGHT = "height"
    WEIGHT = "weight"
    ZABIA_SELF_INTRO = "ZABIA_SELF_INTRO"
    
    
class BotAskingMode(Enum):
    SAY = "SAY"
    ASK = "ASK"
    OPTION = "OPTION"


class UserResponseContentType(Enum):
    TEXT = "text"
    IMAGE = "image"


class QuestionAnswerType(Enum):
    pass



