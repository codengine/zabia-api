from django.db import models
from core.models.base_entity import BaseEntity
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser


class ZUserResponse(BaseEntity):
    zuser = models.ForeignKey(ZUser, on_delete=models.CASCADE)
    conversation = models.ForeignKey(ZabiaConversation, null=True, on_delete=models.CASCADE)
    response_type = models.CharField(max_length=20) # ResponseType.CHOICE_RESPONSE.value
    response_text = models.TextField(default="")

