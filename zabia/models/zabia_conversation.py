from random import randint
from django.db import models
from core.models.base_entity import BaseEntity
from zabia.constants.enums import BotAskingMode, ConversationTag, ConversationType, Presentation
from zabia.models.managers.zabia_introduction_query_manager import ZabiaIntroductionModelManager


class ZabiaConversationText(BaseEntity):
    text = models.TextField()
    text_en = models.TextField(default='')
    description = models.TextField(default='')
    description_en = models.TextField(default='')

    def has_placeholder(self):
        contains_special_chars = lambda x: "{{" in x and "}}" in x
        return contains_special_chars(self.text) or contains_special_chars(self.text_en) or contains_special_chars(self.description) or contains_special_chars(self.description_en)

    def as_json(self):
        return {
            'text': self.text,
            'text_en': self.text_en,
            'description': self.description,
            'description_en': self.description_en
        }
    
    @classmethod
    def add(cls, text, text_en='', description='', description_en=''):
        if any([not text]):
            return
        text_instances = cls.objects.filter(text=text)
        if text_instances.exists():
            return text_instances.first()
        else:
            text_instance = cls()
            
        text_instance.text = text
        if text_en:
            text_instance.text_en = text_en
            
        if description:
            text_instance.description = description
            
        if description_en:
            text_instance.description_en = description_en
            
        text_instance.save()
        
        return text_instance


class ZabiaConversation(BaseEntity):
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)
    texts = models.ManyToManyField(ZabiaConversationText)
    tag = models.CharField(max_length=255)  # ConversationTag.NAME.value
    presentation = models.CharField(max_length=50) # Presentation.LINK.value
    image = models.ImageField(max_length=500, upload_to='profiles/raw/', null=True)
    order = models.IntegerField(default=0)
    conversation_type = models.CharField(max_length=20)  # QuestionType.FIRST_TIME_INTRODUCTION.value
    user_response_type = models.CharField(max_length=20, null=True) # UserResponseType.TEXT_RESPONSE.value
    user_response_content_type = models.CharField(max_length=20, null=True) # UserResponseContentType.TEXT.value
    bot_asking_mode = models.CharField(max_length=20, default=BotAskingMode.SAY.value) # BotAskingMode.SAY.value

    zintroductionmanager = ZabiaIntroductionModelManager()

    def pick_conversation_text(self):
        try:
            texts = self.texts.all()
            text_instance_count = texts.count()
            text_instance = texts[randint(0, text_instance_count - 1)]
            return text_instance
        except Exception as exp:
            pass

    @classmethod
    def exists(cls, tag, conversation_type):
        return cls.objects.filter(tag=tag, conversation_type=conversation_type).exists()

    @classmethod
    def add(cls, texts, tag, conversation_type, presentation,
                     bot_asking_mode, order=0, parent_id=None,
                     image=None, user_response_type=None,
                     user_response_content_type=None):
        if any([not texts, not tag, not conversation_type, not presentation, not bot_asking_mode]):
            return None
        try:
            z_q = cls()
            z_q.tag = tag
            z_q.presentation = presentation
            if order:
                z_q.order = order
            z_q.conversation_type = conversation_type
            if image:
                z_q.image = image
            if user_response_type:
                z_q.user_response_type = user_response_type
            if user_response_content_type:
                z_q.user_response_content_type = user_response_content_type
            z_q.bot_asking_mode = bot_asking_mode
            if parent_id:
                z_q.parent_id = parent_id
            z_q.save()
            z_q.texts.add(*texts)
            return z_q
        except Exception as exp:
            pass

    @classmethod
    def add_zabia_self_introduction(cls, text_list=[], order=1):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                              tag=ConversationTag.ZABIA_SELF_INTRO.value,
                              conversation_type=ConversationType.ZABIA_INTRODUCTION.value,
                              presentation=Presentation.INFO.value,
                              bot_asking_mode=BotAskingMode.SAY.value,
                              order=order)
        return instance

