from django.db import models
from core.models.base_entity import BaseEntity


class AppInfo(BaseEntity):
    app_id = models.CharField(max_length=500)

    @classmethod
    def update_app_info(cls, app_id):
        app_info_objects = cls.objects.filter(app_id=app_id)
        if app_info_objects.exists():
            app_info_object = app_info_objects.first()
        else:
            app_info_object = cls()
            app_info_object.app_id = app_id
            app_info_object.save()
        return app_info_object

    @classmethod
    def get_latest_app_id(cls):
        app_info_instance = cls.objects.all().order_by('-id').first()
        if app_info_instance:
            return app_info_instance.app_id
