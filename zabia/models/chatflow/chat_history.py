from django.db import models
from core.models.base_entity import BaseEntity
from zabia.models.zabia_conversation import ZabiaQuestion
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_response import ZUserResponse


class ChatHistory(BaseEntity):
    user = models.ForeignKey(ZUser)
    triggered_source = models.CharField(max_length=255) # ChatTriggerSource.ZABIA_USER.value
    question = models.ForeignKey(ZabiaQuestion, null=True)
    response = models.ForeignKey(ZUserResponse)