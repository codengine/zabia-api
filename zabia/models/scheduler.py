from django.db import models
from core.models.base_entity import BaseEntity


class Scheduler(BaseEntity):
    name = models.CharField(max_length=255)
