from django.db import models

from zabia.constants.enums import ConversationTag, ConversationType


class ZabiaIntroductionModelManager(models.Manager):

    def get_queryset(self):
        queryset = super(ZabiaIntroductionModelManager, self).get_queryset()

        queryset = queryset.filter(tag = ConversationTag.ZABIA_SELF_INTRO.value,
                                   conversation_type=ConversationType.ZABIA_INTRODUCTION.value,
                                   ).order_by('order')

        return queryset
