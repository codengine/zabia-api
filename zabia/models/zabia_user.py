from decimal import Decimal

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

from core.models.base_entity import BaseEntity


class ZUser(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    image = models.ImageField(max_length=500, upload_to='profiles/raw/', null=True)
    thumbnail = models.ImageField(max_length=500, upload_to='profiles/thumbnails/', null=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    nick_name = models.CharField(max_length=255, null=True, blank=True)
    full_name = models.CharField(max_length=255, null=True, blank=True)
    age = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    gender = models.CharField(max_length=20, null=True, blank=True)
    height = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    weight= models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    terms_accepted = models.BooleanField(default=False)
    email_verified = models.BooleanField(default=False)
    email_verification_code = models.CharField(max_length=500, null=True, blank=True)
    email_verification_code_expiry = models.DateTimeField(null=True, blank=True)

    @classmethod
    def check_if_name_missing(cls,app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(full_name__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_age_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(age__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_gender_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(gender__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_weight_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(weight__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_height_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(height__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_basic_info_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) &
                                     (Q(full_name__isnull=True) | Q(age__isnull=True) |
                                       # Q(weight__isnull=True) | Q(height__isnull=True) |
                                      Q(gender__isnull=True)))
        if z_users.exists():
            return True
        return False

    @classmethod
    def add_new_user(cls, app_id, app_user_id):
        z_user = cls()
        z_user.app_id = app_id
        z_user.app_user_id = app_user_id
        z_user.save()
        return z_user

    @classmethod
    def update_terms_agreed(cls, app_id, app_user_id, terms_accepted):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.terms_accepted = terms_accepted
            z_user.save()
            return z_user
        return None

    @classmethod
    def update_name(cls, app_id, app_user_id, name):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.full_name = name
            z_user.save()
            return z_user
        return None

    @classmethod
    def update_gender(cls, app_id, app_user_id, gender):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.gender = gender
            z_user.save()
            return z_user
        return None

    @classmethod
    def update_age(cls, app_id, app_user_id, age):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.age = Decimal(age)
            z_user.save()
            return z_user
        return None

    @classmethod
    def get_name(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            return z_user.full_name
        return None