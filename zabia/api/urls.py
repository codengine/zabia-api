from django.conf.urls import url
from zabia.api.viewsets.zabia_introduction_api_viewset import ZabiaIntroductionAPIView


urlpatterns = [
    url(r'^zabia-self-introduction/$', ZabiaIntroductionAPIView.as_view(), name='zabia_self_introduction'),
]
