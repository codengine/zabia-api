from core.api.viewsets.base_api_viewset import BaseAPIViewSet
from zabia.api.serializers.zabia_self_introduction_serializer import ZabiaSelftIntroConversationSerializer
from zabia.models.zabia_conversation import ZabiaConversation


class ZabiaIntroductionAPIView(BaseAPIViewSet):
    queryset = ZabiaConversation.zintroductionmanager.all()
    serializer_class = ZabiaSelftIntroConversationSerializer

