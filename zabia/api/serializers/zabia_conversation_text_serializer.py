from core.api.serializers.base_model_serializer import BaseModelSerializer
from zabia.models.zabia_conversation import ZabiaConversationText


class ZabiaConversationTextSerializer(BaseModelSerializer):

    class Meta:
        model = ZabiaConversationText
        fields = ('id', 'text', 'text_en', 'description', 'description_en')
