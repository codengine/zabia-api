from __future__ import print_function

import jwt
import smooch
from smooch.rest import ApiException
from config.smooch import smooch_config
from zabia.chatflow.router import ChatRouter
from zabia.models.app_info import AppInfo
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.utils.smooch_parser import SmoochParser


class ChatBot(object):

    def obtain_jwt_token(self):
        token_bytes = jwt.encode({'scope': 'app'}, smooch_config['SECRET'], algorithm='HS256', headers={'kid': smooch_config['KEY_ID']})
        token = token_bytes.decode('utf-8')
        return token

    def __init__(self):
        self.auth_token = self.obtain_jwt_token()
        smooch.configuration.api_key['Authorization'] = self.auth_token
        smooch.configuration.api_key_prefix['Authorization'] = 'Bearer'
        self.api_instance = smooch.ConversationApi()
        self.menu_api_instance = smooch.MenuApi()
        
    def trigger_start_typing_activity(self, app_id, app_user_id):
        typing_activity_trigger_body = smooch.TypingActivityTrigger(role="appMaker", type="typing:start")
        try:
            self.api_instance.trigger_typing_activity(app_id, app_user_id, typing_activity_trigger_body)
        except ApiException as e:
            print("Exception when calling ConversationApi->trigger_typing_activity: %s\n" % e)    
            
    def trigger_stop_typing_activity(self, app_id, app_user_id):
        typing_activity_trigger_body = smooch.TypingActivityTrigger(role="appMaker", type="typing:stop")
        try:
            self.api_instance.trigger_typing_activity(app_id, app_user_id, typing_activity_trigger_body)
        except ApiException as e:
            print("Exception when calling ConversationApi->trigger_typing_activity: %s\n" % e)

    def get_api_instance(self):
        return self.api_instance

    def build_action(self, text, type, payload, uri=None):
        action = smooch.Action(type=type, text=text, payload=payload, uri=uri)
        return action

    def build_message_item(self, title, media_url, media_type, actions=[]):
        message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type, actions=actions)
        return message_item

    def send_message(self, app_id, app_user_id, message_post_body):
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)
        return api_response

    def send_custom_text_message(self, app_id, app_user_id, message_text):
        message_post_body = smooch.MessagePost(
            'appMaker', 'text', text=message_text)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)
        return api_response

    def send_first_hello_message(self, app_id, app_user_id, json_body, skip_message=False, skip_typing_activity_trigger=False):
        if not skip_typing_activity_trigger:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        if skip_message:
            actions = []
            action = smooch.Action(type='postback', text="I Agree with That", payload="AGREED_TERM_AND_POLICY")
            actions += [action]
            msg_text = "Sorry, without your consent, I'm afraid we cannot continue! "
            msg_text += "In case you want to agree click the button below."
            message_post = smooch.MessagePost(text=msg_text, role='appMaker', type='text', actions=actions)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

        else:
            items = []
            actions = []
            action = self.build_action(text='See terms of use', uri='https://www.championtutor.com/online/faq/',
                                       type='link', payload='TERMS_OF_USE')
            actions += [action]
            action = self.build_action(text='See privacy policy', uri='https://www.championtutor.com/online/faq/',
                                       type='link', payload='PRIVACY_POLICY')
            actions += [action]
            action = self.build_action(text='I am okay with that', type='postback', payload='AGREED_TERM_AND_POLICY')
            actions += [action]

            title = 'Hi! This is Zabia \n\n\n\n'
            media_url = 'https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png'
            media_type = 'image/png'
            description = "I'm a health bot. See my terms and policy below. Is this okay with you?"

            message_item = smooch.MessageItem(title=title, description=description, media_url=media_url,
                                                  media_type=media_type, size='large', actions=actions)

            items += [message_item]

            message_post = smooch.MessagePost(role='appMaker', type='carousel', items=items)

            api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post)
        
        if not skip_typing_activity_trigger:
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        return api_response

    def send_main_menu(self, app_id, app_user_id, json_body):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
        if not name:
            name = json_body['appUser']['givenName']

        options = ['Hello', 'Hi', 'Hey']

        from random import randint

        option = options[randint(0, len(options) - 1)]

        message_text = '%s! How can I help you please?' % option
        message_text += 'Below are your options. Select one of them or just ask me something'
        message_post_body = smooch.MessagePost(
            'appMaker', 'text', text=message_text)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)

        items = []
        # One message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='FORMA_ACTIVARSE')
        actions += [action]

        item = self.build_message_item(title='Formas de activarse',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/activarse.png',
                                       media_type='image/png', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='YOGA')
        actions += [action]

        item = self.build_message_item(title='Yoga',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/contigo_mismo.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='MEDITACION')
        actions += [action]

        item = self.build_message_item(title='Meditaci�n',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/meditacion1.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='TIP_ACTIVARSE')
        actions += [action]

        item = self.build_message_item(title='Tips',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/tip_consejo.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                          items=items)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post)
        
        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        return api_response

    def handle_basic_info_missing(self, app_id, app_user_id, json_body):
        if ZUser.check_if_name_missing(app_id=app_id, app_user_id=app_user_id):
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

            question = "One more thing: Don’t use me in medical emergencies. I don’t "
            question += "provide medical advice, and I don’t support emergency calls"
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=question)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)

            name = json_body.get('appUser', {}).get('givenName', '')

            question = "How do I call you %s? %s, Is it your name?" % (name, name)

            actions = []
            action = smooch.Action(type='reply', text='Yes, This is my name',
                                   payload='BASIC_INFO:NAME_CORRECT')
            actions += [action]

            action = smooch.Action(type='reply', text='No, Update My Name',
                                   payload='BASIC_INFO:UPDATE_MY_NAME')
            actions += [action]

            message_post = smooch.MessagePost(role='appMaker', type='text', text=question,
                                              actions=actions)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif ZUser.check_if_gender_missing(app_id=app_id, app_user_id=app_user_id):
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
            question = "Thank you so much %s. What is your gender?" % (name,)
            actions = []
            action = smooch.Action(type='reply', text='Male',
                                   payload='BASIC_INFO:GENDER_MALE')
            actions += [action]

            action = smooch.Action(type='reply', text='Female',
                                   payload='BASIC_INFO:GENDER_FEMALE')
            actions += [action]

            message_post = smooch.MessagePost(role='appMaker', type='text', text=question,
                                              actions=actions)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif ZUser.check_if_age_missing(app_id=app_id, app_user_id=app_user_id):
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
            question = "How old are you?"

            message_post = smooch.MessagePost(role='appMaker', type='text', text=question)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

            ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                           q_postback="BASIC_INFO:AGE")

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif ZUser.check_if_weight_missing(app_id=app_id, app_user_id=app_user_id):
            pass
        elif ZUser.check_if_height_missing(app_id=app_id, app_user_id=app_user_id):
            pass
        else:
            self.send_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

    def handle_terms_not_accepted(self, app_id, app_user_id, json_body):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body, skip_message=True, skip_typing_activity_trigger=True)
        
        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

    def handle_request(self, request):
        smooch_parser = SmoochParser(request)
        json_body = smooch_parser.get_json_body()
        app_id = smooch_parser.get_app_id()
        app_user_id = smooch_parser.get_app_user_id()

        AppInfo.update_app_info(app_id=app_id)

        # from zabia.handlers.menu_handler import MenuHandler
        #
        # MenuHandler().add_persistent_menu()

        return self.handle_message(app_id, app_user_id, json_body)

    def handle_introductory_messages(self, app_id, app_user_id, json_body):
        pass

    def handle_message(self, app_id, app_user_id, json_body):
        if json_body.get('trigger') == 'message:appUser':

            smooch_payload = json_body['messages'][0].get('payload')

            smooch_payload = smooch_payload if smooch_payload else ''

            if not smooch_payload:
                new_user = ChatRouter.detect_if_new_user(app_user_id)

                if new_user:
                    # Save the new user
                    z_user = ZUser.add_new_user(app_id=app_id, app_user_id=app_user_id)
                    return self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                elif not ChatRouter.detect_if_terms_accepted(app_id=app_id, app_user_id=app_user_id):
                    return self.handle_terms_not_accepted(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                elif ZUserInputAwait.check_if_waiting(app_id=app_id, app_user_id=app_user_id):
                    waiting_on_postback = ZUserInputAwait.get_current_waiting_postback(app_id=app_id, app_user_id=app_user_id)
                    message_type = json_body['messages'][0]['type']
                    message_text = json_body['messages'][0]['text']

                    if waiting_on_postback == "BASIC_INFO:AGE":
                        if message_type == 'text':
                            try:
                                message_text = message_text.replace("years", "").replace("year", "").strip()
                                message_text = int(message_text)
                                ZUser.update_age(app_id=app_id, app_user_id=app_user_id, age=message_text)

                                ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

                                return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id,
                                                                      json_body=json_body)
                            except Exception as exp:
                                print(str(exp))
                                msg = "Please enter your age(e.g 25 or 25 years or 25 year etc)"
                                self.send_custom_text_message(app_id, app_user_id, msg)

                    elif waiting_on_postback == "BASIC_INFO:UPDATE_MY_NAME":
                        if message_type == 'text':
                            input_text = json_body['messages'][0].get('text')
                            ZUser.update_name(app_id=app_id, app_user_id=app_user_id, name=input_text)
                            ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

                            msg = "Thank you so much! Let us help with some more information"
                            self.send_custom_text_message(app_id, app_user_id, msg)

                            return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id,
                                                                  json_body=json_body)

                elif ChatRouter.detect_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id):
                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                    if not name:
                        name = json_body['appUser']['givenName']

                    options = ['Hello', 'Hi', 'Hey']

                    from random import randint

                    option = options[randint(0, len(options) - 1)]
                    message_text = '%s! How can I help you please? Before we can proceed please ' % option
                    message_text += 'help us with some of your basic information'
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                    return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                else:
                    message_type = json_body['messages'][0]['type']
                    message_text = json_body['messages'][0]['text']

                    if message_type == 'text':
                        return self.send_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            else:
                # waiting_for_input = ZUserInputAwait.check_if_waiting(app_id=app_id, app_user_id=app_user_id)
                # if waiting_for_input:
                #     if smooch_payload == "BASIC_INFO:UPDATE_MY_NAME":
                #         input_text = json_body['messages'][0].get('text')
                #         ZUser.update_name(app_id=app_id, app_user_id=app_user_id, name=input_text)
                #         ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)
                #
                #         msg = "Thank you so much! Let us help with some more information"
                #         self.send_custom_text_message(app_id, app_user_id, msg)
                #
                #         return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id,
                #                                               json_body=json_body)

                if smooch_payload == "BASIC_INFO:NAME_CORRECT":
                    name = json_body.get('appUser', {}).get('givenName', '')
                    ZUser.update_name(app_id=app_id, app_user_id=app_user_id, name=name)
                    return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                elif smooch_payload == "BASIC_INFO:UPDATE_MY_NAME":
                    msg = "Ok then how do we call you?"
                    self.send_custom_text_message(app_id, app_user_id, msg)
                    ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                   q_postback="BASIC_INFO:UPDATE_MY_NAME")
                elif smooch_payload == "BASIC_INFO:GENDER_MALE":
                    ZUser.update_gender(app_id=app_id, app_user_id=app_user_id, gender="male")
                    ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                    msg = "Okay %s, now we know your gender information" % name
                    self.send_custom_text_message(app_id, app_user_id, msg)

                    return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id,
                                                          json_body=json_body)

                elif smooch_payload == "BASIC_INFO:GENDER_FEMALE":
                    ZUser.update_gender(app_id=app_id, app_user_id=app_user_id, gender="female")
                    ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                    msg = "Okay %s, now we know your gender information" % name
                    self.send_custom_text_message(app_id, app_user_id, msg)

                    return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id,
                                                          json_body=json_body)

        elif json_body.get('trigger') == 'postback':
            postback_payload = json_body['postbacks'][0]['action']['payload']

            if postback_payload == "AGREED_TERM_AND_POLICY":
                ZUser.update_terms_agreed(app_id=app_id, app_user_id=app_user_id, terms_accepted=True)

                msg = "Thank you so much! Let us help with some basic information"
                self.send_custom_text_message(app_id, app_user_id, msg)

                return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            elif postback_payload == "BASIC_INFO:UPDATE_MY_NAME":
                ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                               q_postback="BASIC_INFO:UPDATE_MY_NAME")

