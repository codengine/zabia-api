from zabia.models.zabia_user import ZUser


class ChatRouter(object):

    def __init__(self, json_body):
        self.json_body = json_body

    @classmethod
    def detect_if_new_user(self, app_user_id):
        return not ZUser.objects.filter(app_user_id=app_user_id).exists()

    @classmethod
    def detect_if_terms_accepted(cls, app_id, app_user_id):
        z_user = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id).first()
        if z_user:
            return z_user.terms_accepted
        return None

    @classmethod
    def detect_if_basic_info_missing(cls, app_id, app_user_id):
        return ZUser.check_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id)




