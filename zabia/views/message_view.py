import json
import smooch
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponseRedirect, HttpResponse
from django.utils.decorators import method_decorator
from core.views.base_view import BaseView
from zabia.bot import Bot
from zabia.chatbot import ChatBot
from zabia.models.app_info import AppInfo
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.models.zuser_response import ZUserResponse



class MessageView(BaseView):
    def get(self, request, *args, **kwargs):
        if request.GET['hub.verify_token'] == 'VERIFY_TOKEN':
            return HttpResponse(request.GET['hub.challenge'])
        else:
            return HttpResponse('Invalid verify token')

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            chatbot = ChatBot()
            message_replied = chatbot.handle_request(request)
        except Exception as exp:
            pass
        return HttpResponse({'a': 1}, content_type='application/json')

    # @method_decorator(csrf_exempt)
    # def post1(self, request, *args, **kwargs):
    #     try:
    #         bot = Bot()
    #         api_instance = bot.get_api_instance()
    #
    #         body = request.body.decode('utf-8')
    #         json_body = json.loads(body)
    #         #author_id = json_body['messages']['authorId']
    #         message_replied = bot.handle_message(json_body)
    #         print(message_replied)
    #     except Exception as exp:
    #         print(str(exp))
    #     return HttpResponse({'a': 1}, content_type='application/json')