from core.views.base_view import BaseView
from django.http import HttpResponse

from zabia.models.scheduler import Scheduler


class HomeView(BaseView):
    def get(self, request, *args, **kwargs):
        import celery
        Scheduler
        return HttpResponse("Hello! Zabia is here to help")