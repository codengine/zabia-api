import requests


class HttpRequestHandler(object):

    @classmethod
    def perform_post_request(cls, url, data):
        try:
            response = requests.post(url=url, json=data)
            print(response.json())
            return response.json()
        except Exception as exp:
            print(str(exp))
            pass

    @classmethod
    def perform_api_request(cls, url, **kwargs):
        try:
            response = requests.get(url=url).json()
            return response
        except Exception as exp:
            pass
