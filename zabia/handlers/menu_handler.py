import smooch
import jwt
from config.smooch import smooch_config
from zabia.models.app_info import AppInfo


class MenuHandler(object):

    def obtain_jwt_token(self):
        token_bytes = jwt.encode({'scope': 'app'}, smooch_config['SECRET'], algorithm='HS256', headers={'kid': smooch_config['KEY_ID']})
        token = token_bytes.decode('utf-8')
        return token

    def add_persistent_menu(self):
        self.auth_token = self.obtain_jwt_token()
        smooch.configuration.api_key['Authorization'] = self.auth_token
        smooch.configuration.api_key_prefix['Authorization'] = 'Bearer'
        self.menu_api_instance = smooch.MenuApi()

        mitems = []
        mitem = smooch.MenuItem(text="Main Menu", type="postback", payload="PM_MAIN")
        mitems += [mitem]
        mitem = smooch.MenuItem(text="Update Profile", type="postback", payload="PM_UPDATE_PROFILE")
        mitems += [mitem]
        mitem = smooch.MenuItem(text="Settings", type="postback", payload="PM_SETTINGS")
        mitems += [mitem]
        # mitem = smooch.MenuItem(text="See Terms of Use", type="uri", uri='', payload="PM_TERMS_OF_USE")
        # mitems += [mitem]
        # mitem = smooch.MenuItem(text="See Privacy Policy", type="uri", uri='', payload="PM_PRIVACY_POLICY")
        # mitems += [mitem]
        menu = smooch.Menu(items=mitems)

        app_id = AppInfo.get_latest_app_id()
        if app_id:
            api_response = self.menu_api_instance.update_menu(app_id, menu)
            print(api_response)

