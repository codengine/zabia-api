from __future__ import print_function

import jwt
import smooch
from smooch.rest import ApiException
from config.smooch import smooch_config


class Bot(object):

    def obtain_jwt_token(self):
        token_bytes = jwt.encode({'scope': 'app'}, smooch_config['SECRET'], algorithm='HS256', headers={'kid': smooch_config['KEY_ID']})
        token = token_bytes.decode('utf-8')
        return token

    def __init__(self):
        self.auth_token = self.obtain_jwt_token()
        smooch.configuration.api_key['Authorization'] = self.auth_token
        smooch.configuration.api_key_prefix['Authorization'] = 'Bearer'
        self.api_instance = smooch.ConversationApi()

    def get_api_instance(self):
        return self.api_instance

    def build_action(self, text, type, payload):
        action = smooch.Action(type=type, text=text, payload=payload)
        return action

    def build_message_item(self, title, media_url, media_type, actions=[]):
        message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type, actions=actions)
        return message_item

    def handle_message(self, json_body):
        try:
            # print(type(json_body))
            app_id = json_body.get('app', {}).get('_id')
            app_user_id = json_body.get('appUser', {}).get('_id')

            user_name = json_body.get('appUser', {}).get('givenName', '')

            salute = 'Hola ' + user_name;

            print(json_body.get('trigger'))

            if json_body.get('trigger') == 'message:appUser':

                smooch_payload = json_body['messages'][0].get('payload')

                print("Smooch Payload")
                print(smooch_payload)

                smooch_payload = smooch_payload if smooch_payload else ''

                if not smooch_payload:
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text = salute + ', ¿en que te gustaria enfocarte hoy?')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    print(api_response)

                    items = []
                    # One message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='SENTIRSE_BIEN')
                    actions += [action]

                    item = self.build_message_item(title='Sentirse bien', media_url='https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png',
                                                    media_type='image/png', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    item = self.build_message_item(title='Mantenerse activo',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/mantenerseactivo.png',
                                                   media_type='image/png', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='COMER_SANO')
                    actions += [action]

                    item = self.build_message_item(title='Comer sano',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/mandarinas.png',
                                                   media_type='image/png', actions=actions)
                    items += [item]

                    message_post = smooch.MessagePost(role='appMaker', type='carousel', items=items)

                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)
                else:
                    if smooch_payload == "SI":
                        items = []
                        actions = []
                        action = self.build_action(text='Seleccionar', type='postback', payload='SENTIRSE_BIEN')
                        actions += [action]

                        item = self.build_message_item(title='Sentirse bien',
                                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png',
                                                       media_type='image/png', actions=actions)
                        items += [item]

                        # Next message item
                        actions = []
                        action = self.build_action(text='Seleccionar', type='postback', payload='MANTENERSE_ACTIVO')
                        actions += [action]

                        item = self.build_message_item(title='Mantenerse activo',
                                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/mantenerseactivo.png',
                                                       media_type='image/png', actions=actions)
                        items += [item]

                        # Next message item
                        actions = []
                        action = self.build_action(text='Seleccionar', type='postback', payload='COMER_SANO')
                        actions += [action]

                        item = self.build_message_item(title='Comer sano',
                                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/mandarinas.png',
                                                       media_type='image/png', actions=actions)
                        items += [item]

                        message_post = smooch.MessagePost(role='appMaker', type='carousel', items=items)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                    elif smooch_payload == "NO":
                        message_text = "Ok, Gracias " + user_name + ", aquí estoy siempre! Bye"

                        message_post = smooch.MessagePost(role='appMaker', type='text', text=message_text)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                    elif smooch_payload == "TIPO_RECETA":
                        items = []
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Ver receta", payload="RECETA_1")
                        
                        title = "Papas asadas al horno"
                        description = "35.22 kcal."
                        size = "large"
                        media_url = "https://southtech.pe/zabiatest/imagen/receta/a49a0841acee748bc2b18d02ad86cd94.jpg"
                        media_type = "image/jpg"
                        message_item = smooch.MessageItem(title=title, description=description, size=size, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Ver receta", payload="RECETA_2")
                        
                        title = "Salmón a la barbacoa"
                        description = "333.90 kcal."
                        media_url = "https://southtech.pe/zabiatest/imagen/receta/bbq-salmon-267647.jpg"
                        media_type = "image/jpg"
                        message_item = smooch.MessageItem(title=title, description=description, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Ver receta", payload="RECETA_3")
                        
                        title = "Filetes de salmón con Soja y miel de Maple"
                        description = "228.40 kcal."
                        media_url = "http://southtech.pe/zabiatest/imagen/receta/salmon-steaks-with-soy-maple-glaze-4628.jpg"
                        media_type = "image/jpg"
                        message_item = smooch.MessageItem(title=title, description=description, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Ver receta", payload="RECETA_4")
                        
                        title = "Pasta sin Gluten Con Espárragos"
                        description = "295.43 kcal."
                        media_url = "https://southtech.pe/zabiatest/imagen/receta/pasta_with_asparagus-38028.jpg"
                        media_type = "image/jpg"
                        message_item = smooch.MessageItem(title=title, description=description, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Ver receta", payload="RECETA_5")
                        
                        title = "Helado cremoso de Limón"
                        description = "320.86 kcal."
                        media_url = "https://southtech.pe/zabiatest/imagen/receta/Key-Lime-Ice-Cream-452186.jpg"
                        media_type = "image/jpg"
                        message_item = smooch.MessageItem(title=title, description=description, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        message_post = smooch.MessagePost(role='appMaker', type='list', items=items)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                            
                    elif smooch_payload == "SI_GUSTA_RECETA":
                        message_post = smooch.MessagePost(role='appMaker', type='text', text="Muchas Gracias por tu feedback! Lo tendremos en cuenta")

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                            
                    elif smooch_payload == "RETORNO_PRINCIPAL":
                        items = []
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Seleccionar", payload="SENTIRSE_BIEN")
                        
                        title = "Sentirse bien"
                        media_url = "https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png"
                        media_type = "image/png"
                        message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Seleccionar", payload="MANTENERSE_ACTIVO")
                        
                        title = "Mantenerse activo"
                        media_url = "https://www.southtech.pe/zabiatest/imagen/icono/mantenerseactivo.png"
                        media_type = "image/png"
                        message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        actions = []
                        action = smooch.Action(type="postback", text="Seleccionar", payload="COMER_SANO")
                        
                        title = "Comer sano"
                        media_url = "https://www.southtech.pe/zabiatest/imagen/icono/mandarinas.png"
                        media_type = "image/png"
                        message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type)
                        items += [message_item]
                        
                        message_post = smooch.MessagePost(role='appMaker', type='carousel', items=items)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                            
                    else:
                        message, image = '', ''
                        if smooch_payload == "FELIZ":
                            message = "Que Bien! " + user_name + ", Sentirse Feliz es estar bien con uno mismo en cada momento."
                            image = "1.png"
                        elif smooch_payload == "TRISTE":
                            message = user_name + ", Hay momentos negativos en nuestras vidas que no vamos a poder controlar pero si podemos elegir como no sentimos al respecto"
                            image = "2.png"
                        elif smooch_payload == "MIEDO":
                            message = "El Miedo es una sensación desagradable, " + user_name + ", provocada por la percepción de un peligro real o imaginario"
                            image = "3.png"
                        elif smooch_payload == "IRA":
                            message = user_name + ", La Ira es un sentimiento negativo y lo mejor es que la exterioricemos de alguna manera para evitar que devenga una enfermedad"
                            image = "4.png"
                        elif smooch_payload == "NORMAL":
                            image = "5.png"
                        
                        message_text = message + "\n\nUna buena alimentación, ejercicios físicos y / o actividades recreacionales, te ayudan a tu estado anímico y a Sentirse Bien "
                        message_post = smooch.MessagePost(role='appMaker', type='text', text=message_text)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                            
                        media_url = "'https://southtech.pe/zabiatest/imagen/icono/recomendacion/" + image
                        message_post = smooch.MessagePost(role='appMaker', type='image', media_url=media_url)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                            
                        actions = []
                        action = smooch.Action(type='reply', text='Si',
                                           payload='SI')
                        actions += [action]
                        
                        action = smooch.Action(type='reply', text='No',
                                           payload='NO')
                        actions += [action]
                        
                        message_post = smooch.MessagePost(role='appMaker', type='text', text="¿Te puedo ayudar en algo más?", actions=actions)

                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post)
                        

            elif json_body.get('trigger') == 'postback':
                postback_payload = json_body['postbacks'][0]['action']['payload']
                print(json_body['postbacks'])
                if postback_payload == 'SENTIRSE_BIEN':
                    actions = []
                    action = smooch.Action(type='reply', text='Feliz',
                                           icon_url='https://www.southtech.pe/zabiatest/imagen/icono/feliz.png',
                                           payload='FELIZ')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Triste',
                                           icon_url='https://www.southtech.pe/zabiatest/imagen/icono/triste.png',
                                           payload='TRISTE')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Con miedo',
                                           icon_url='https://www.southtech.pe/zabiatest/imagen/icono/miedo.png',
                                           payload='MIEDO')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Con ira',
                                           icon_url='https://www.southtech.pe/zabiatest/imagen/icono/ira.png',
                                           payload='IRA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Normal',
                                           icon_url='https://www.southtech.pe/zabiatest/imagen/icono/normal.png',
                                           payload='NORMAL')
                    actions += [action]

                    message_post = smooch.MessagePost(role='appMaker', type='text', text='¿Cómo te sientes hoy?',
                                                      actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)

                elif postback_payload == 'COMER_SANO':
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text='Excelente ' + user_name + ', ¿Déjame ver cómo te puedo apoyar?')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    items = []
                    # One message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='SITIO')
                    actions += [action]

                    item = self.build_message_item(title='Sitios',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/sitio.jpg',
                                                   media_type='image/jpg', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='TIP')
                    actions += [action]

                    item = self.build_message_item(title='Tips',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/loginzan.png',
                                                   media_type='image/png', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='RECETA')
                    actions += [action]

                    item = self.build_message_item(title='Recetas',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/pitahayasalad.jpg',
                                                   media_type='image/jpg', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='NUTRIENTE')
                    actions += [action]

                    item = self.build_message_item(title='Nutrientes',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/Avocado_facts.jpeg',
                                                   media_type='image/jpeg', actions=actions)
                    items += [item]

                    message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                                      items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)
                elif postback_payload == 'MANTENERSE_ACTIVO':
                    message_text = "%s, La actividad física puede hacer mucho más que ayudarlo a mantenerse en forma," % user_name
                    message_text += "también puede mejorar su salud en general, como por ejemplo:\naliviar el estrés,"
                    message_text += "mejorar el sueño, fortalecer los huesos y músculos, hacerlo sentir lleno de energía,"
                    message_text += "desarrollar fuerza y resistencia, hacerlo sentir bien consigo mismo."
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    items = []
                    # One message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='FORMA_ACTIVARSE')
                    actions += [action]

                    item = self.build_message_item(title='Formas de activarse',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/activarse.png',
                                                   media_type='image/png', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='YOGA')
                    actions += [action]

                    item = self.build_message_item(title='Yoga',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/contigo_mismo.jpeg',
                                                   media_type='image/jpeg', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='MEDITACION')
                    actions += [action]

                    item = self.build_message_item(title='Meditación',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/meditacion1.jpeg',
                                                   media_type='image/jpeg', actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='TIP_ACTIVARSE')
                    actions += [action]

                    item = self.build_message_item(title='Tips',
                                                   media_url='https://www.southtech.pe/zabiatest/imagen/icono/tip_consejo.jpeg',
                                                   media_type='image/jpeg', actions=actions)
                    items += [item]

                    message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                                      items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)

                elif postback_payload == 'SITIO':
                    items = []

                    actions = []
                    action = self.build_action(text='Aromas Peruanos!', type='postback', payload='SITIO_FIN')
                    actions += [action]

                    title = 'Aromas Peruanos'
                    description = """Buffet con más de treinta y cinco platos criollos y marinos.\nAv. Guardia Civil 856, San Isidro"""
                    size = "large"
                    media_url = "https://www.southtech.pe/zabiatest/imagen/restaurante/aromas-peruanos-logo_side.jpg"
                    media_type = "image/jpg"

                    message_item = smooch.MessageItem(title=title, description=description, media_type=media_type,
                                                      size=size, media_url=media_url, actions=actions)

                    items += [message_item]

                    actions = []
                    action = self.build_action(text='Astrid y Gastón!', type='postback', payload='SITIO_FIN')
                    actions += [action]

                    title = 'Astrid y Gastón'
                    description = """El toque de Astrid y Gastón\nAv. Paz Soldán 290, San Isidro"""
                    media_url = "https://southtech.pe/zabiatest/imagen/restaurante/astrid-y-gaston-logo_side.jpg"
                    media_type = "image/jpg"

                    message_item = smooch.MessageItem(title=title, description=description, media_type=media_type,
                                                      media_url=media_url, actions=actions)

                    items += [message_item]

                    actions = []
                    action = self.build_action(text='Mara biomarket!', type='postback', payload='SITIO_FIN')
                    actions += [action]

                    title = 'Mara biomarket'
                    description = "Productos orgánicos y naturales.\nAv. Camino Real 1251, San Isidro "
                    media_url = "https://southtech.pe/zabiatest/imagen/biotienda/mara.jpg"
                    media_type = "image/jpg"

                    message_item = smooch.MessageItem(title=title, description=description, media_type=media_type,
                                                      media_url=media_url, actions=actions)

                    items += [message_item]

                    message_post = smooch.MessagePost(role='appMaker', type='list',
                                                      items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='Si')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == 'TIP':
                    message_text = "Me inspire un poco, te dejo TIPS de Vida saludable!  Buen Dia!"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image', media_url='https://southtech.pe/zabiatest/imagen/icono/recomendacion/23.png')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='Si')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == 'RECETA':
                    actions = []
                    action = smooch.Action(type='reply', text='Vegana',
                                           payload='TIPO_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Vegetariana',
                                           payload='TIPO_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Pescetariana',
                                           payload='TIPO_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='Ninguna',
                                           payload='TIPO_RECETA')
                    actions += [action]

                    message_text = "Buenísimo! permíteme saber si tienes alguna preferencia"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "RECETA_1":
                    message_text = "Papas asadas al horno"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image',
                        media_url='https://southtech.pe/zabiatest/imagen/receta/a49a0841acee748bc2b18d02ad86cd94.jpg')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='SI_GUSTA_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "Sancochar las papas con cascara por 10 minutos, hasta que estén ligeramente cocidas.\n"
                    message_text += "Trozarlas en 4 partes, ponerlas sobre una fuente para el horno, rocear con aceite de oliva extra virgen y espolvorear con especias y sal gruesa.\n"
                    message_text += "Poner al horno precalentado por 20 minutos. Servir a gusto."
                    message_text += "1 Papa Blanca trozada\n"
                    message_text += "1 cucharadita de romero\n"
                    message_text += "1 cucharadita de sal gruesa\n"
                    message_text += "1 cucharada de aceite de oliva\n"
                    message_text += "¿Te gustó?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "RECETA_2":
                    message_text = "Salmón a la barbacoa"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image',
                        media_url='https://southtech.pe/zabiatest/imagen/receta/bbq-salmon-267647.jpg')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='SI_GUSTA_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "Una vez precalentada la barbacoa, a fuego medio alto colocar el salmón sobre la parrilla que "
                    message_text += "previamente habremos untado con aceite, sal y pimienta. Si se cocinan lomos, poner el lado sin piel "
                    message_text += "primero, pero si nos gusta la piel crujiente, podremos primero el lado con piel. Una vez en la parrilla, "
                    message_text += "lo dejaremos tocándolo lo menos posible. Un lomo de unos 3 centímetros de grosor se cocina en 6 a "
                    message_text += "10 minutos. unos 4 o 5 minutos por lado, Cuando está hecho, tenemos que retirarlo del fuego y dejarlo "
                    message_text += "reposar. echarle generosamente salsa Barbecue.\n"
                    message_text += "Salmon Fresco  1 kg.\n"
                    message_text += "Aceite de Oliva 2 cdta.\n"
                    message_text += "pimienta negra 1 cdta.\n"
                    message_text += "salsa barbecue 1 taza.\n"
                    message_text += "¿Te gustó?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "RECETA_3":
                    message_text = "Filetes de salmón con Soja y miel de Maple"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image',
                        media_url='http://southtech.pe/zabiatest/imagen/receta/salmon-steaks-with-soy-maple-glaze-4628.jpg')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='SI_GUSTA_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "En un recipiente, ponga una cucharada de salsa de Soja o Sillau, junto con "
                    message_text += 'la Miel de Maple, y el aceite "de ajonlí.\n'
                    message_text += "Agregue el Salmón y voolteelo para que se marine.Incorpore el jenjibre y ajos rallados en ambos lados del pescado.\n"
                    message_text += "Cubra y refrigere por 2 horas, volteando el Salmón varias veces.\n"
                    message_text += "Encienda el carbón en la parrilla, aceite un poco e incorpore el salmón marinado, "
                    message_text += "verifique que el fuego esté moderado, y dorelo a gusto por dos minutos en cada lado.Reserve los "
                    message_text += "jugos de la marinada.\n"
                    message_text += "Ponga los jugos de la marinada en un pequeña olla y hiervalo por unos 3 minutos hasta que se "
                    message_text += "espese en una salsa., viertalo en un recipiente.\n"
                    message_text += "Sirvalo con unos cebollines a la parrila.\n"
                    message_text += "Salmón Rosa 500 gr.\n"
                    message_text += "aceite de oliva 2 cucharadas.\n"
                    message_text += "cebolla china 1 taza.\n"
                    message_text += "Ajos trozados 1 cucharada.\n"
                    message_text += "Jengibre o Kión 1 cucharada.\n"
                    message_text += "Miel de Maple 1 taza.\n"
                    message_text += "¿Te gustó?"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "RECETA_4":
                    message_text = "Pasta sin Gluten Con Espárragos"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image',
                        media_url='https://southtech.pe/zabiatest/imagen/receta/pasta_with_asparagus-38028.jpg')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='SI_GUSTA_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='NO')
                    actions += [action]

                    message_text = "Hierva la pasta por 8 minutos,\n"
                    message_text += "Agregue los esparragos salteados en aceite de olive,\n"
                    message_text += "clara de huevo y ajos,\n"
                    message_text += "mezcle y ralle el queso parmesano.\n"
                    message_text += "Sirva y disfrute!.\n"

                    message_text += "Espárragos en lata.\n"
                    message_text += "1 yema de huevo.\n"
                    message_text += "Aceite de Oliva virgen 1 cda.\n"
                    message_text += "Dientes de Ajo 3 unidades.\n"
                    message_text += "Queso Parmesano Reggiano rayado 1/4 taza.\n"
                    message_text += "Pasta sin Gluten 1 / 2 kilo.\n"
                    message_text += "Sal de mesa  1 cdta.\n"
                    message_text += "Pimienta negra 1/2 cdta.\n"

                    message_text += "¿Te gustó?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "RECETA_5":
                    message_text = "Helado cremoso de Limón"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image',
                        media_url='https://southtech.pe/zabiatest/imagen/receta/Key-Lime-Ice-Cream-452186.jpg')
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = smooch.Action(type='reply', text='Si',
                                           payload='SI_GUSTA_RECETA')
                    actions += [action]

                    action = smooch.Action(type='reply', text='NO',
                                           payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Batir los claras y yemas, azúcar, jugo del limón y la ralladura de limón en un recipiente sobre la "
                    message_text += "hornilla a temperatura media hasta que esté bien batido y uniforme. Mover continuamente la mezcla "
                    message_text += "con una espátula de madera hasta que espese, por aproximadamente 7 a 8 minutos. La mezcla debe "
                    message_text += "estar espesa para que cubra la cuchara.\n"
                    message_text += "Remueva del fuego, siga moviendo y agregue la crema de leche hasta que este suave e uniforme."
                    message_text += "Cuele la mezcla con un colador fino sobre un recipiente. Cubra y deje enfriar en el "
                    message_text += "refrigerador, moviéndolo ocasionalmente hasta que enfríe por aprox una hora.Vierta la "
                    message_text += "mezcla fría en una cubeta de hacer helados o en una recipiente a prueba de frió y "
                    message_text += "congelen hasta lograr la consistencia deseada.\n"
                    message_text += "Transfiera el helado a contenedor, cubra con film y póngale la tapa.Para mejores "
                    message_text += "resultados, el helado debe mantenerse en la congeladora por mínimo 2 horas o toda la noche antes de consumir.\n"

                    message_text += "4Yemas de huevo\n"
                    message_text += "2 claras de huevos\n"
                    message_text += "2 1/4 taza de crema de leche\n"
                    message_text += "1 cucharada de rayadura de limón\n"
                    message_text += "3/4 taza de jugo de limón\n"
                    message_text += "1 1/4 taza de azúcar blanca.\n"

                    message_text += "¿Te gustó?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "NUTRIENTE":
                    message_text = "Los Nutrientes son parte importante en la esencia de la vida y regeneracion celular. ¿qué grupos de NUTRIGENÓMICOS te gustaria explorar?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    items = []
                    # One message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='ANTIINFLAMATORIO')
                    actions += [action]

                    item = smooch.MessageItem(title="Antiinflamatorio", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='ANTIOXIDANTE')
                    actions += [action]

                    item = smooch.MessageItem(title="Antioxidantes", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='BELLEZA')
                    actions += [action]

                    item = smooch.MessageItem(title="Belleza", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='ESTADO_ANIMICO')
                    actions += [action]

                    item = smooch.MessageItem(title="Estado anímico", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='FUERZA')
                    actions += [action]

                    item = smooch.MessageItem(title="Fuerza", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='MEMORIA')
                    actions += [action]

                    item = smooch.MessageItem(title="Memoria", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='PERDIDA_PESO')
                    actions += [action]

                    item = smooch.MessageItem(title="Pérdida de peso", actions=actions)
                    items += [item]

                    # Next message item
                    actions = []
                    action = self.build_action(text='Seleccionar', type='postback', payload='PREVENCION_ENFERMEDAD')
                    actions += [action]

                    item = smooch.MessageItem(title="Prevención de enfermedades", actions=actions)
                    items += [item]

                    message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                                      items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)

                elif postback_payload == "ANTIINFLAMATORIO":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Antiinflamatorio:\n"
                    message_text += "Ácido fólico\n"
                    message_text += "Antocianidinas\n"
                    message_text += "Carotenoides\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                elif postback_payload == "ANTIOXIDANTE":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Antioxidante: \n"
                    message_text += "Betacaroteno\n"
                    message_text += "Catequinas\n"
                    message_text += "Curcumina\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "BELLEZA":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Belleza: \n"
                    message_text += "Licopeno\n"
                    message_text += "Luteína y zeaxantina\n"
                    message_text += "Omega 3\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "ESTADO_ANIMICO":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Estado anímico:\n"
                    message_text += "Antocianidinas\n"
                    message_text += "Catequinas\n"
                    message_text += "Curcumina\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "FUERZA":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Fuerza: \n"
                    message_text += "Magnesio\n"
                    message_text += "Omega 3\n"
                    message_text += "Potasio\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "MEMORIA":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Memoria: \n"
                    message_text += "Potasio\n"
                    message_text += "Selenio\n"
                    message_text += "Vitamina B12\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "PERDIDA_PESO":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='RETORNO_PRINCIPAL')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Pérdida de peso: \n"
                    message_text += "Curcumina\n"
                    message_text += "Fibra\n"
                    message_text += "Potasio\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "PREVENCION_ENFERMEDAD":
                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='NUTRIENTE')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='')
                    actions += [action]

                    message_text = "Gracias, estas son los mejores nutrientes para Prevención de enfermedades: \n"
                    message_text += "Curcumina\n"
                    message_text += "Fibra\n"
                    message_text += "Fitoestrógenos\n"

                    message_text += "¿Desea validar otro nutriente?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "FORMA_ACTIVARSE":
                    items = []

                    actions = []
                    action = self.build_action(text='Tenga prisa!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Tenga prisa."
                    description = "Caminar rápidamente quema más calorías que un paseo lento. Convierta en un juego el ver qué tan rápido puede llegar a su destino"
                    size = "large"
                    message_item = smooch.MessageItem(title=title, description=description, size=size, actions=actions)
                    items += [message_item]

                    actions = []
                    action = self.build_action(text='De pié!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Póngase los zapatos de pié."
                    description = "Intente ponerse el calcetín, el zapato y atar sus agujetas sin permitir que su pie toque el suelo."
                    message_item = smooch.MessageItem(title=title, description=description, actions=actions)
                    items += [message_item]

                    actions = []
                    action = self.build_action(text='Escaleras', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Tome las escaleras."
                    description = "Subir escaleras es una de las actividades más fáciles que puede hacer para quemar calorías sin tener que ir al gimnasio."
                    message_item = smooch.MessageItem(title=title, description=description, actions=actions)
                    items += [message_item]

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'carousel', items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)



                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "YOGA":
                    message_text = "Nos encanta el YOGA, aca te compartimos algunos ejercicios que puedes desarrollar todos los días."
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)


                    items = []

                    actions = []
                    action = self.build_action(text='Contigo mismo!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = user_name + ", conéctate contigo mismo!"
                    media_url = "https://southtech.pe/zabiatest/imagen/icono/contigo_mismo.jpeg"
                    media_type = "image/jpeg"
                    size = "large"

                    message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type, size=size, actions=actions)

                    items += [message_item]

                    actions = []
                    action = self.build_action(text='Busca tu centro!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Estirate, oxigenate y busca tu centro"
                    description = "Te sentiras muy bien luego."
                    media_url = "https://southtech.pe/zabiatest/imagen/icono/busca_tu_centro.jpeg"
                    media_type = "image/jpeg"

                    message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type,
                                                      description=description, actions=actions)

                    items += [message_item]

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'list', items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "MEDITACION":
                    message_text = "Una excelente manera de activar tus sentidos y novelar tus emociones el dia de hoy, no dejes de practicarlo a diario"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    items = []

                    actions = []
                    action = self.build_action(text='Silencio y meditación!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Silencio y meditación"
                    media_url = "https://southtech.pe/zabiatest/imagen/icono/meditacion1.jpeg"
                    media_type = "image/jpeg"
                    size = "large"

                    message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type,
                                                      size=size, actions=actions)

                    items += [message_item]

                    actions = []
                    action = self.build_action(text='Minfulness!', type='postback', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    title = "Minfulness"
                    media_url = "https://southtech.pe/zabiatest/imagen/icono/meditacion2.jpeg"
                    media_type = "image/jpeg"

                    message_item = smooch.MessageItem(title=title, media_url=media_url, media_type=media_type, actions=actions)

                    items += [message_item]

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'list', items=items)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                elif postback_payload == "TIP_ACTIVARSE":
                    message_text = "Con gusto " + user_name + ", este es nuestro mejor consejo para ti el dia de hoy!"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    media_url = "https://southtech.pe/zabiatest/imagen/icono/tip_consejo.jpeg"
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'image', media_url=media_url)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    actions = []
                    action = self.build_action(text='Si', type='reply', payload='MANTENERSE_ACTIVO')
                    actions += [action]

                    action = self.build_action(text='No', type='reply', payload='NO')
                    actions += [action]

                    message_text = "¿Te puedo ayudar en algo más?"

                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text=message_text, actions=actions)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                else:
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text', text="implementando...")
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

            return True
        except Exception as exp:
            print(str(exp))
            return False
        
        
        
        