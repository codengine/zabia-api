import json


class SmoochParser(object):

    def __init__(self, request):
        self.request = request
        self.json_body = {}
        self.parse(request)

    def parse(self, request):
        body = request.body.decode('utf-8')
        self.json_body = json.loads(body)

    def get_json_body(self):
        return self.json_body

    def get_app_id(self):
        app_id = self.json_body.get('app', {}).get('_id')
        return app_id

    def get_app_user_id(self):
        app_user_id = self.json_body.get('appUser', {}).get('_id')
        return app_user_id

    def get_username(self):
        user_name = self.json_body.get('appUser', {}).get('givenName', '')
        return user_name
