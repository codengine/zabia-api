from zabia.constants.enums import QuestionType, Presentation, QuestionTag
from zabia.models.zabia_conversation import ZabiaQuestion


def initialize_basic_questions():
    question = "One more thing: Don’t use me in medical emergencies. I don’t "
    question += "provide medical advice, and I don’t support emergency calls"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.INFO.value
    q_type = QuestionType.BASIC_INFO.value

    q1 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type, presentation=presentation)

    if not q1:
        print("Q1 adding failed")
        return

    question = "How do I call you [Name]? [Name], Is it your name?"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.ACTIONS.value
    q_type = QuestionType.BASIC_INFO.value

    q2 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q1.pk)

    if not q2:
        print("Q2 adding failed")
        return

    question = "Yes, This is my name"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.REPLY
    q_type = QuestionType.BASIC_INFO.value

    q3 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q2.pk)

    if not q3:
        print("Q3 adding failed")
        return

    question = "Update My Name"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.REPLY
    q_type = QuestionType.BASIC_INFO.value

    q4 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q2.pk)

    if not q4:
        print("Q4 adding failed")
        return

    question = "So, just to check, [Name] sounds like a [Gender] name. Is that right??"
    q_tag = QuestionTag.GENDER.value
    presentation = Presentation.REPLY.value
    q_type = QuestionType.BASIC_INFO.value

    q6 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q3.pk)

    if not q6:
        print("Q6 adding failed")
        return

    question = "Yes"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.REPLY
    q_type = QuestionType.BASIC_INFO.value

    q7 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q6.pk)

    if not q7:
        print("Q7 adding failed")
        return

    question = "No"
    q_tag = QuestionTag.NAME.value
    presentation = Presentation.REPLY
    q_type = QuestionType.BASIC_INFO.value

    q8 = ZabiaQuestion.add_question(q_text=question, q_tag=q_tag, q_type=q_type,
                                    presentation=presentation, parent_id=q6.pk)

    if not q8:
        print("Q8 adding failed")
        return

