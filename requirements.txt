celery==4.2.0
Django==2.0.6
PyJWT==1.6.4
requests==2.18.4
smooch==3.8.0
